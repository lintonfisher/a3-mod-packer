# Vars
$currentDir = Get-Location
$modDir = "$currentDir\@testing"
$pboConsole = "C:\Program Files\PBO Manager v.1.4 beta\PBOConsole.exe"

# Function to print to the screen with the current time and severity level
Function Write-LogOutput {
    Param(
        [Parameter(Mandatory = $True)]
        [ValidateSet("INFO", "WARNING", "ERROR", "DEBUG", "EXEC")]
        [string]$Sev,
        [Parameter(Mandatory = $True)]
        [string]$Message,
        [string]$LogFile = "$currentDir\psoutput.log"
    )

    if ($(Test-Path -Path $LogFile) -eq $false) {
        New-Item -Path $LogFile -ItemType File
    }

    $Timestamp = Get-Date -Format "HH:mm:ss"
    Switch ($Sev) {
        "INFO" { $Colour = "Green"; }
        "WARNING" { $Colour = "Yellow"; }
        "ERROR" { $Colour = "Red"; }
        "DEBUG" { $Colour = "Magenta"; }
        "EXEC" { $Colour = "Cyan"; }
    }

    # Only print DEBUG output if -DebugMode was passed
    If ((($Sev -eq "DEBUG") -and $DebugMode) -or ($Sev -ne "DEBUG")) {
        $output = "$Timestamp`t$Sev`t$Message"
        Write-Host -ForegroundColor $Colour -Object $output
        Add-Content -Path $logFile -Value $output
    }
}

if ($(Test-Path -Path $modDir) -eq $false) {
    New-Item -Path $modDir -ItemType Directory
    @'
name = "My Testing Mod";
description = "My Testing Mod";
tooltip = "My Testing Mod";
tooltipOwned = "My Testing Mod";
overview = "My Testing Mod";
author = "Me";
overviewText = "My Testing Mod";
overviewFootnote = "My Testing Mod";
'@ | Out-File -FilePath "$modDir\mod.cpp"
    Write-LogOutput -Sev INFO -Message "Created missing mod directory: $modDir"
}

$items = Get-ChildItem -Path $currentDir | Where-Object { $_.PSIsContainer } | Select-Object Name,FullName

# Pack folders into PBO
foreach ($item in $items) {
    if ($item.Name -ne '@testing' -and $item.Name -ne 'ps') {
        Write-LogOutput -Sev DEBUG -Message "Processing $($item.FullName)"
        Write-LogOutput -Sev EXEC -Message "$pboConsole -pack $($item.FullName) $($item.FullName).pbo"
        & $pboConsole -pack $item.FullName "$($item.FullName).pbo" *>$null
        Move-Item -Path "$($item.FullName).pbo" -Destination "$modDir\$($item.Name).pbo" -Force
        Write-LogOutput -Sev INFO "Packed $($item.Name) -> $modDir\$($item.Name).pbo"
    } else {
        Write-LogOutput -Sev DEBUG -Message "Skipped $($item.FullName)"
    }
}

# Cleanup PBOs
Write-LogOutput -Sev INFO -Message "Would you like to cleanup PBOs that no longer have a project folder? (Y/n)"
$userAction = $Host.UI.RawUI.ReadKey()
if ($userAction.Character -eq "y") {
    $pbos = Get-ChildItem -Path "$modDir" -Filter "*.pbo"

    foreach ($pbo in $pbos) {
        Write-LogOutput -Sev DEBUG -Message $pbo.FullName
        if ($items.Name -notcontains $pbo.Name.Replace('.pbo', '')) {
            Remove-Item -Path $pbo.FullName -Force
            Write-LogOutput -Sev INFO -Message "Removed orphaned $($pbo.FullName)"
        }
    }
}
Write-LogOutput -Sev INFO -Message "Done!"