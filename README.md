# A3 Mod Packer

A3 Mod Packer is a set of scripts to aid in the packing of in-development mods and running ARMA3.

## Prerequisites

- PowerShell _(if you're on Windows 10 you should have this already)_.
- PBOConsole.exe from [PBOManager](https://www.armaholic.com/page.php?id=16369) is used to pack the folders into PBOs.

## Installation

Simply download or clone this repository to your working folder.

## Usage

1. Add any unpacked mods you wish to test.
1. Execute `pack_files.bat`
1. Execute `run_arma.bat`

### Example

Before running pack_files.bat your file structure should look like this:

```
a3-mod-packer
├─ my_mod_1
|  └─ your_mod_files
├─ ps
├─ pack_files.bat
└─ run_arma.bat
```

After running pack_files.bat your file structure should look like this:

```
a3-mod-packer
├─ @testing
|  ├─ addons
|  |  └─ my_mod_1.pbo
|  └─ mod.cpp
├─ my_mod_1
|  └─ your_mod_files
├─ ps
├─ pack_files.bat
└─ run_arma.bat
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License

[MIT](https://choosealicense.com/licenses/mit/)